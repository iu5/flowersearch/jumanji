#!/usr/bin/env bash
conda create --name "$1" python=3.7 -y
conda init bash
source /opt/conda/etc/profile.d/conda.sh
conda activate "$1"
conda install -c anaconda jupyter -y
conda install ipykernel -y
python -m ipykernel install --user --name "$1" --display-name "Python ($1)"
pip install -r requirements.txt