import argparse
import yaml

from jumanji.config import PipelineConfig, TrainerConfig, ClassBox
from jumanji.callbacks import ModelCheckpoint

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config_path", type=str, help="Path to .yml file with configuration parameters ."
    )
    config_path = parser.parse_args().config_path

    config_yaml = yaml.load(open(config_path, "r"), Loader=yaml.FullLoader)
    pipeline_params = PipelineConfig(**config_yaml["pipeline"])
    trainer_params = TrainerConfig(**config_yaml["trainer"])

    logger_params = trainer_params.logger
    trainer_params.logger = ClassBox.loggers[logger_params.name](**logger_params.params)

    callbacks_params_list = trainer_params.callbacks
    if callbacks_params_list:
        trainer_params.callbacks = [ClassBox.callbacks[callbacks_params.name](**callbacks_params.params)
                                    for callbacks_params in callbacks_params_list]

    trainer_params.checkpoint_callback = ModelCheckpoint(**trainer_params.checkpoint_callback)

    pipeline = ClassBox.pipelines[pipeline_params.name](
        models_params=pipeline_params.models,
        criterions_params=pipeline_params.criterions,
        optimizers_params=pipeline_params.optimizers,
        schedulers_params=pipeline_params.schedulers,
        metrics_params=pipeline_params.metrics,
        data_params=pipeline_params.data,
    )
    import torch

    pipeline.load_state_dict(torch.load("/experiments/efnetb0-1channel-v0.ckpt")["state_dict"])
    model = pipeline.models.model
    model.model.set_swish(memory_efficient=False)
    traced = torch.jit.trace(model.eval(), (torch.rand(2, 1, 224, 224)))
    torch.jit.save(traced, "efnetb0_torch15.pt")

