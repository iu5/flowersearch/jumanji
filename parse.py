from collections import namedtuple

import torch
from torch.nn import Module
from torch.utils.data import WeightedRandomSampler
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from albumentations.core.composition import BaseCompose

from jumanji.config import PipelineConfig, TrainerConfig, ClassBox


def parse_transforms(tranforms_yaml, tranforms_dict):
    transform_name = tranforms_yaml["name"]
    transform_class = tranforms_dict[transform_name]
    if issubclass(transform_class, BaseCompose):
        params = tranforms_yaml["params"]
        transforms = params.pop("transforms")
        transforms = [parse_transforms(transform, tranforms_dict) for transform in transforms]
        return transform_class(transforms=transforms, **params)
    else:
        return transform_class(**tranforms_yaml.get("params", {}))


def parse_pipeline(pipeline_config):
    pipeline_config = PipelineConfig(**pipeline_config)
    parsed_models = {
        model_name: ClassBox.models[model.name](**model.params)
        for model_name, model in pipeline_config.models.items()
    }

    models = Module()
    for model_name, model in parsed_models.items():
        models.__setattr__(model_name, model)

    criterions = {
        criterion_name: ClassBox.criterions[criterion.name](**criterion.params)
        for criterion_name, criterion in pipeline_config.criterions.items()
    }

    criterions = namedtuple("criterions", criterions.keys())(**criterions)

    metrics = {
        metric_name: ClassBox.metrics[metric.name](**metric.params)
        for metric_name, metric in pipeline_config.metrics.items()
    }

    metrics = namedtuple("metrics", metrics.keys())(**metrics)

    optimizers = [ClassBox.optimizers[optimizer.name](models.parameters(), **optimizer.params)
                  for optimizer in pipeline_config.optimizers]

    schedulers = [ClassBox.optimizers[scheduler.name](models.parameters(), **scheduler.params)
                  for scheduler in pipeline_config.schedulers]

    data = parse_data(pipeline_config.data)
    print(data)
    pipeline = ClassBox.pipelines[pipeline_config.name](models, optimizers, schedulers, criterions, metrics, data)
    return pipeline


def parse_data(data):
    train_loader = configure_data(data.train)
    eval_loader = configure_data(data.eval)
    test_loader = configure_data(data.test) if data.test else None

    return train_loader, eval_loader, test_loader


def parse_trainer(trainer_config):
    trainer_params = TrainerConfig(**trainer_config)
    if trainer_params.callbacks:
        trainer_params.callbacks = [ClassBox.callbacks[callbacks_params.name](**callbacks_params.params)
                                    for callbacks_params in trainer_params.callbacks]

    trainer_params.logger = ClassBox.loggers[trainer_params.logger.name](**trainer_params.logger.params)

    trainer_params.checkpoint_callback = ModelCheckpoint(**trainer_params.checkpoint_callback)
    trainer = Trainer(**trainer_params.dict())

    return trainer


def configure_data(data_params):
    transform = data_params.dataset.params.get("transform")
    if transform:
        data_params.dataset.params["transform"] = parse_transforms(data_params.dataset.params["transform"],
                                                                   ClassBox.transforms)
    augment = data_params.dataset.params.get("augment")
    if augment:
        data_params.dataset.params["augment"] = parse_transforms(augment, ClassBox.transforms)
    dataset = ClassBox.datasets[data_params.dataset.name](**data_params.dataset.params)

    # TODO: Add universal sampler
    sampler = data_params.dataloader.params.get("sampler")
    if sampler:
        data_params.dataloader.params["sampler"] = prepare_sampler(dataset)
    return ClassBox.dataloaders[data_params.dataloader.name](
        dataset, **data_params.dataloader.params
    )


def prepare_sampler(dataset):
    class_count = dataset.csv.label.value_counts()
    class_count = [class_count[label] for label in range(len(class_count))]

    target_list = torch.LongTensor(dataset.csv.label)
    target_list = target_list[torch.randperm(len(target_list))]

    class_weights = 1 - torch.Tensor(class_count) / sum(class_count)

    class_weights_all = class_weights[target_list]

    weighted_sampler = WeightedRandomSampler(
        weights=class_weights_all,
        num_samples=len(class_weights_all),
        replacement=True
    )

    return weighted_sampler
