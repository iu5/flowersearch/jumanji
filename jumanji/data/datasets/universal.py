from pathlib import Path
from typing import Union, Optional

import cv2
import pandas as pd
import torch
from torch.utils.data import Dataset
from albumentations import BasicTransform
from albumentations.core.composition import BaseCompose


class ImageDataset(Dataset):
    def __init__(
            self,
            csv_path: Path,
            data_folder: Path,
            transform: Optional[Union[BasicTransform, BaseCompose]] = None,
            augment: Optional[Union[BasicTransform, BaseCompose]] = None,
    ):
        self.data_folder = Path(data_folder)
        self.csv = pd.read_csv(csv_path, index_col=0)

        self.csv["path"] = self.csv["path"].map(
            lambda path: self.data_folder.joinpath(path)
        )

        self.transform = transform
        self.augment = augment

    def __getitem__(self, item):
        record = self.csv.iloc[item]
        image = self.read_image(record.path)
        if self.augment:
            image = self.augment(image=image)["image"]

        tensor = self.transform(image=image)["image"]
        label = torch.LongTensor([record.label])

        return tensor, label

    def __len__(self):
        return self.csv.shape[0]

    @staticmethod
    def read_image(path):
        return cv2.imread(str(path))