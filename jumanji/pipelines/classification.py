import torch

from .base import Pipeline


class ClassificationPipeline(Pipeline):
    def forward(self, x):
        return self.models.model(x)

    def training_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def validation_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def test_step(self, batch, batch_nb):
        return self._step(batch, batch_nb)

    def _step(self, batch, batch_idx):
        x, y = batch
        pred = self.forward(x)
        loss = self.criterions.bce(pred, y)
        # loss = self.criterions.focal(pred, y.to(dtype=torch.int64))

        y_pred = (pred > 0.5).int()

        return {"loss": loss, "y_pred": y_pred, "y_true": y, "y_scores": pred}

    def training_epoch_end(self, outputs):
        return self._epoch_end(outputs, "train")

    def validation_epoch_end(self, outputs):
        return self._epoch_end(outputs, "valid")

    def test_epoch_end(self, outputs):
        return self._epoch_end(outputs, "test")

    def _epoch_end(self, outputs, mode):
        avg_loss = torch.stack([x["loss"] for x in outputs]).mean()

        y_true = torch.cat([x["y_true"] for x in outputs]).int().squeeze(-1)
        y_pred = torch.cat([x["y_pred"] for x in outputs]).squeeze(-1)
        y_scores = torch.cat([x["y_scores"] for x in outputs]).squeeze(-1)

        f1 = self.metrics.f1(y_pred, y_true)

        ppv = self.metrics.ppv(y_scores, y_true)

        logs = {f"{mode}_avg_loss": avg_loss, f"{mode}_f1": f1, f"{mode}_ppv": ppv}

        self.log_dict(logs, logger=True)

        self.trainer.logger.experiment.log_confusion_matrix(
            y_true=y_true.cpu(),
            y_predicted=y_pred.cpu(),
            title=f"{mode}",
            file_name=f"{mode}.json",
            overwrite=True
        )


class MetricLearningPipeline(ClassificationPipeline):
    def forward(self, x):
        features = self.models.backbone(x)
        out = self.models.head(features)
        return out

    def _step(self, batch, batch_idx):
        x, y = batch
        out = self.forward(x)
        loss = self.criterions.classification(out, y.squeeze(-1))
        pred = out.argmax(-1)

        return {"loss": loss,
                "y_pred": pred,
                "y_true": y}

    def _epoch_end(self, outputs, mode):
        avg_loss = torch.stack([x["loss"] for x in outputs]).mean()

        y_true = torch.cat([x["y_true"] for x in outputs]).squeeze(-1)
        y_pred = torch.cat([x["y_pred"] for x in outputs]).squeeze(-1)

        f1 = self.metrics.f1(y_pred, y_true)

        logs = {f"{mode}_avg_loss": avg_loss, f"{mode}_f1": f1}
        self.log_dict(logs, logger=True)
