from pytorch_lightning import LightningModule


class Pipeline(LightningModule):
    def __init__(
            self,
            models,
            optimizers,
            schedulers,
            criterions,
            metrics,
            data,
    ):
        super().__init__()
        self.models = models
        self.optimizers = optimizers
        self.schedulers = schedulers
        self.criterions = criterions
        self.metrics = metrics
        self.data = data

    def configure_optimizers(self):
        return self.optimizers, self.schedulers

    def train_dataloader(self):
        return self.data[0]

    def val_dataloader(self):
        return self.data[1]

    def test_dataloader(self):
        return self.data[2]
