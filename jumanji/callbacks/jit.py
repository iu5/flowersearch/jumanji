import torch

from . import Callback


class SaveEncoderJit(Callback):
    def __init__(self, save_to):
        self.save_to = save_to

    def on_train_end(self, trainer, pl_module):
        example_forward_input = torch.rand(1, 3, 224, 224)
        torch_script = torch.jit.trace(pl_module.models.backbone.cpu().eval(), example_forward_input)
        torch_script.save("encoder.pt")
