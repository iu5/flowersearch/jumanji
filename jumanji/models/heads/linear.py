from torch import nn


class LinearHead(nn.Module):
    def __init__(self, in_features, num_classes, with_softmax=True):
        super().__init__()
        layers = [nn.Linear(in_features, num_classes)]
        if with_softmax:
            layers.append(nn.Softmax())

        self.fc = nn.Sequential(*layers)

    def forward(self, inputs):
        return self.fc(inputs)
