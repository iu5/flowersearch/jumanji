from .efficientnet_pytorch import EfficientNetModel

from .heads import LinearHead
