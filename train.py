import argparse
import yaml

from pytorch_lightning import Trainer

from jumanji.config import PipelineConfig, TrainerConfig, ClassBox
from parse import parse_pipeline, parse_trainer

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config_path", type=str, help="Path to .yml file with configuration parameters ."
    )
    config_path = parser.parse_args().config_path
    config_yaml = yaml.load(open(config_path, "r"), Loader=yaml.FullLoader)

    pipeline = parse_pipeline(config_yaml["pipeline"])
    trainer = parse_trainer(config_yaml["trainer"])

    trainer.fit(pipeline)
    trainer.test(pipeline)
